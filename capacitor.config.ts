import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'dragondev.tips.app',
  appName: 'Tips App Dragon Guru',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
