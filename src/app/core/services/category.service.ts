import { Injectable } from '@angular/core';
import { Category } from 'src/app/shared/tips.interface';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  categories: Category[] = [
    // {
    //   id: 'comer-sano',
    //   name: 'Comer sano',
    //   subtitle: '¡Cuida de ti mismo y disfruta de una alimentación balanceada!',
    //   img: '/assets/images/categories/dieta.png'
    // },
    {
      id: 'habitos',
      name: 'Hábitos',
      subtitle: '¡Inténtalos y observa cómo impactan en tu vida!',
      img: '/assets/images/categories/lifestyle.png'
    },
    {
      id: 'hablar-en-pulico',
      name: 'Hablar en público',
      subtitle: 'La práctica constante y la confianza en ti mismo son esenciales.',
      img: '/assets/images/categories/diplomatico.png'
    },
    {
      id: 'para-padres',
      name: 'Para padres',
      subtitle: '¡Cuida de tus hijos y disfruta de esta maravillosa experiencia!',
      img: '/assets/images/categories/padre-e-hija.png'
    },
    {
      id: 'motivacion',
      name: 'Motivación',
      subtitle: '¡Tú puedes lograrlo! 💪🌟',
      img: '/assets/images/categories/motivacion.png'
    },
  ];
  categorySelected: Category | undefined;

  constructor() { }

  get getCategories() {
    return this.categories;
  }

  get getCategorySelected() {
    return this.categorySelected;
  }

  set setCategorySelected(category: Category) {
    this.categorySelected = category;
  }
}
