import { Injectable } from '@angular/core';
import { Channel, LocalNotifications } from '@capacitor/local-notifications';
import { Platform } from '@ionic/angular';
import * as dayjs from 'dayjs';

type ActionsLocalNotify =
  | 'OPEN_MESSAGE'
  | 'VIEW_POST';

type LiteralUnion<T extends U, U = string> = T | (U & Record<never, never>);

type ActionsNotification = LiteralUnion<ActionsLocalNotify, string>;

@Injectable({
  providedIn: 'root'
})

export class LocalNotificationsService {

  // Capacitor declarations
  localNotifications = LocalNotifications;
  actionsNotify = {
    openMsg: 'OPEN_MESSAGE',
    openPost: 'VIEW_POST'
  };

  constructor(
    private platform: Platform
  ) { }

  /**
   * @description
   * Init local notifications
   */
  initLocalNotifications(){
    this.localNotificationReceivedListener();
    this.localNotificationActionPerformedListener();
    this.createaActionTypes();
    // ********************************
    if(this.platform.is('android')){
      this.createNotificationChannel();
    }
  }

  /**
   * @description
   * Create channel with priority to show local notifications
   */
  async createNotificationChannel(){
    const notificationChannel: Channel  = {
      id: 'pop-notifications',// id must match android/app/src/main/res/values/strings.xml's default_notification_channel_id
      name: 'Pop notifications',
      description: 'Pop notifications',
      importance: 5,
      visibility: 1,
      lights: true,
      vibration: true,
    };
    const channel = await this.localNotifications.createChannel(notificationChannel);
    console.log("🚀 ~ LocalNotificationsService ~ createNotificationChannel ~ channel:", channel)
  }

  /**
   * @description
   * Crete action types to local Notifications
   */
  createaActionTypes(){
    this.localNotifications.registerActionTypes({
      types: [
        {
          id: this.actionsNotify.openMsg,
          actions: [{
              id: this.actionsNotify.openMsg,
              title: 'Ver mensaje'
            }]
        },
      ]
    })
  }

  /**
   * @description
   * Set a new local notification
   * @param {String} params.title Add a title to the notification
   * @param {String} params.body Add a short description
   * @param {Number} params.id Add a id to the notification
   * @param {ActionsNotification} params.action Add an action to tap notfication
   * @param {String} params.extraData Add additional data to the notification
   */
  async addLocalNotification(params:{
      title: string,
      body: string,
      id: number,
      action: ActionsNotification,
      extraData: any
  }){
    const {title,body,id,action,extraData} = params;

    await this.localNotifications.schedule({
      notifications: [{
          title,
          body,
          largeBody: body,
          id,
          schedule: { at: new Date(dayjs().add(1, 'second').format()), repeats: false },
          actionTypeId: action,
          extra: extraData,
          // channelId : 'pop-notifications'
        }]
    });
  }

  /**
   * @description
   * Add listener to get information about all local notifications received
   */
  localNotificationReceivedListener(){
    this.localNotifications.addListener('localNotificationReceived', (notification) => {
      console.log("🚀 ~ LocalNotificationsService ~ this.localNotifications.addListener ~ notification:", notification)
      // notification
    })
  }

  /**
   * @description
   * Add listener to press some options of local notification
   */
  localNotificationActionPerformedListener(){
    this.localNotifications.addListener('localNotificationActionPerformed', (data)=>{
      if(data.actionId == 'tap'){
        // ToDo: Add function to tap notify
      }
    });
  }

  /**
   * @description
   * Remove all local notification  s
   */
  async clearLocalNotifications(){
    const { notifications } = await this.localNotifications.getPending();
    this.localNotifications.cancel({notifications});
  }
}
