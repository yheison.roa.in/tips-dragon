import { Tip } from "../tips.interface";

export const tipsData: Tip[] = [
  {
    id: '1',
    title: 'Incorpora una variedad de alimentos',
    description: 'Consumir una amplia gama de alimentos te proporciona una variedad de nutrientes necesarios para una buena salud. Incluye frutas, verduras, granos enteros, proteínas magras y grasas saludables en tu dieta diaria.',
    category_id: 'comer-sano'
  },
  {
    id: '2',
    title: 'Controla las porciones',
    description: 'Presta atención al tamaño de las porciones y trata de no excederte. Utiliza platos más pequeños, lee las etiquetas de los alimentos y familiarízate con las porciones recomendadas.',
    category_id: 'comer-sano'
  },
  {
    id: '3',
    title: 'Prioriza alimentos frescos y naturales',
    description: 'Opta por alimentos frescos y sin procesar siempre que sea posible. Estos suelen ser más ricos en nutrientes y tienen menos aditivos y conservantes.',
    category_id: 'comer-sano'
  },
  {
    id: '4',
    title: 'Limita el consumo de alimentos procesados y ultraprocesados',
    description: 'Los alimentos altamente procesados suelen contener cantidades excesivas de grasas saturadas, azúcares añadidos y sodio. Intenta reducir la ingesta de alimentos como bocadillos empaquetados, comidas rápidas y alimentos precocinados.',
    category_id: 'comer-sano'
  },
  {
    id: '5',
    title: 'Aumenta el consumo de frutas y verduras',
    description: 'Estos alimentos son ricos en vitaminas, minerales, fibra y antioxidantes. Trata de incluir una variedad de colores en tus comidas para asegurarte de obtener una amplia gama de nutrientes.',
    category_id: 'comer-sano'
  },
  {
    id: '6',
    title: 'Elige fuentes magras de proteínas',
    description: 'Opta por proteínas magras como pollo, pavo, pescado, tofu, legumbres y productos lácteos bajos en grasa. Limita el consumo de carnes rojas y procesadas.',
    category_id: 'comer-sano'
  },
  {
    id: '7',
    title: 'Controla el consumo de azúcares añadidos',
    description: 'Reduce la ingesta de bebidas azucaradas, dulces y productos horneados. Opta por alternativas más saludables como frutas frescas, edulcorantes naturales como la miel o el jarabe de arce, y elige productos con bajo contenido de azúcares añadidos.',
    category_id: 'comer-sano'
  },
  {
    id: '8',
    title: 'Bebe suficiente agua',
    description: 'Mantente bien hidratado bebiendo suficiente agua a lo largo del día. Limita el consumo de bebidas azucaradas y refrescos, ya que pueden agregar calorías vacías a tu dieta.',
    category_id: 'comer-sano'
  },
  {
    id: '9',
    title: 'Lee las etiquetas de los alimentos',
    description: 'Familiarízate con la información nutricional en las etiquetas de los alimentos y busca productos con ingredientes simples y bajos en grasas saturadas, azúcares añadidos y sodio.',
    category_id: 'comer-sano'
  },
  {
    id: '10',
    title: 'Planifica y prepara tus comidas con anticipación',
    description: 'Dedica tiempo a planificar tus comidas y meriendas para la semana, y prepara porciones saludables con anticipación. Esto te ayudará a tomar decisiones más saludables cuando tengas hambre y estés ocupado.',
    category_id: 'comer-sano'
  },
  {
    id: '11',
    title: 'Establece metas claras y alcanzables',
    description: `Define qué hábitos deseas desarrollar o cambiar y establece metas específicas y realistas para lograrlos.

    <strong>Identificación de Hábitos y Metas:</strong>

    Reflexiona sobre los hábitos que deseas adoptar o modificar. ¿Son hábitos relacionados con la salud, la productividad, el aprendizaje o las relaciones personales?
    Evalúa por qué estos hábitos son importantes para ti y cómo pueden impactar positivamente en tu vida.

    <strong>Establecimiento de Metas Específicas:</strong>

    Define metas claras y medibles. Por ejemplo, si deseas hacer más ejercicio, establece una meta como “Caminar 30 minutos al día, 5 días a la semana”.
    Asegúrate de que tus metas sean alcanzables y realistas. Establece expectativas que se ajusten a tu estilo de vida y capacidades actuales.

    <strong>Planificación para el Éxito:</strong>

    Desarrolla un plan de acción detallado. ¿Qué pasos concretos tomarás para alcanzar tus metas?
    Considera posibles obstáculos y cómo los superarás. ¿Necesitarás apoyo de amigos o familiares? ¿Hay recursos o herramientas que puedan ayudarte?

    <strong>Seguimiento y Ajuste:</strong>

    Mantén un registro de tu progreso. Esto puede ser a través de una aplicación, un diario o incluso un calendario.
    Sé flexible y dispuesto a ajustar tus metas si las circunstancias cambian o si encuentras desafíos inesperados.

    <strong>Celebración de Logros:</strong>

    Reconoce y celebra tus éxitos, no importa cuán pequeños sean. Esto te motivará a mantener tus hábitos a largo plazo.
    Recuerda que el desarrollo o cambio de hábitos es un proceso continuo que requiere compromiso y paciencia. Sé amable contigo mismo y reconoce cada paso que das hacia tus metas.`,
    category_id: 'habitos'
  },
  {
    id: '12',
    title: 'Comienza pequeño',
    // description: 'En lugar de intentar cambiar todo de una vez, comienza con pequeños cambios incrementales. Esto hace que sea más fácil adaptarse y mantener el impulso a largo plazo.',
    description: `La Clave para el Cambio Sostenible

    Cuando se trata de desarrollar nuevos hábitos o cambiar los existentes, la simplicidad y la consistencia son tus mejores aliados. Aquí te presentamos una guía paso a paso para aplicar este principio:

    <strong>1. Define un Objetivo Claro:</strong>

    Identifica un solo hábito que quieras cambiar o desarrollar. Por ejemplo, si deseas ser más activo, tu objetivo podría ser “dar 10,000 pasos al día”.

    <strong>2. Desglosa tu Meta en Acciones Pequeñas:</strong>

    Divide tu objetivo en tareas pequeñas y manejables. Siguiendo el ejemplo anterior, podrías comenzar con “dar 1,000 pasos después de la cena”.

    <strong>3. Establece Rutinas Simples:</strong>

    Incorpora la nueva acción en tu rutina diaria de una manera que sea fácil de recordar y realizar. Podrías decidir dar esos pasos mientras escuchas tu podcast favorito cada noche.

    <strong>4. Celebra los Pequeños Éxitos:</strong>

    Cada vez que completes la acción, reconócete a ti mismo. Esto puede ser tan simple como marcar un calendario o darte una palmada en la espalda.

    <strong>5. Aumenta Gradualmente:</strong>

    Una vez que la acción pequeña se convierta en un hábito, incrementa el desafío de manera gradual. Puedes aumentar tus pasos a 2,000 y así sucesivamente.

    <strong>6. Sé Paciente y Perseverante:</strong>

    No te desanimes si no ves resultados inmediatos. El cambio de hábitos es un proceso y requiere tiempo y esfuerzo continuos.

    <strong>7. Ajusta Según Sea Necesario:</strong>

    Si encuentras que una acción no funciona para ti, no tengas miedo de ajustarla. La flexibilidad es crucial para encontrar lo que mejor se adapta a tu vida.

    <strong>8. Busca Apoyo:</strong>

    Comparte tus metas con amigos o familiares que puedan ofrecerte apoyo y motivación.

    <strong>9. Evalúa y Refina:</strong>

    Revisa periódicamente tu progreso y haz los cambios necesarios para seguir avanzando hacia tu meta.
    Recuerda, el cambio duradero rara vez ocurre de la noche a la mañana. Al comenzar pequeño y construir gradualmente sobre tus éxitos, estás sentando las bases para una transformación a largo plazo que puede perdurar.`,
    category_id: 'habitos'
  },
  {
    id: '13',
    title: 'Crea una rutina',
    // description: 'Incorpora tus nuevos hábitos en tu rutina diaria. La consistencia es clave para convertir un comportamiento en un hábito arraigado.',
    description: `El Fundamento de los Hábitos Duraderos

    La incorporación de nuevos hábitos en tu vida diaria puede ser un desafío, pero establecer una rutina sólida puede facilitar este proceso. Aquí te presentamos cómo puedes hacerlo:

    <strong>1. Identifica Momentos Clave en tu Día:</strong>

    Observa tu rutina actual y encuentra momentos en los que puedas integrar el nuevo hábito sin que se sienta como una carga. Por ejemplo, si quieres leer más, podrías hacerlo durante tu viaje matutino.

    <strong>2. Asocia Hábitos Nuevos con Actividades Existentes:</strong>

    Vincula el nuevo hábito con una actividad que ya realizas regularmente. Si tu objetivo es beber más agua, intenta tomar un vaso cada vez que termines una tarea en el trabajo.

    <strong>3. Prepara tu Entorno para el Éxito:</strong>

    Asegúrate de que tu entorno esté organizado de tal manera que apoye tu nuevo hábito. Si deseas hacer ejercicio por la mañana, prepara tu equipo la noche anterior.

    <strong>4. Establece Recordatorios Visuales:</strong>

    Coloca notas adhesivas, establece alarmas o utiliza aplicaciones que te recuerden realizar la nueva actividad hasta que se convierta en una parte natural de tu día.

    <strong>5. Mantén la Simplicidad:</strong>

    No compliques tu rutina con hábitos demasiado elaborados o que requieran mucho tiempo. Comienza con acciones simples que puedan convertirse fácilmente en comportamientos automáticos.

    <strong>6. Sé Consistente con los Horarios:</strong>

    Realiza el nuevo hábito a la misma hora todos los días. Esto ayuda a tu cerebro a asociar ese momento específico con la actividad, fortaleciendo la rutina.

    <strong>7. Ajusta tu Rutina Según Sea Necesario:</strong>

    Si descubres que ciertos aspectos de tu rutina no funcionan, no dudes en hacer ajustes. La adaptabilidad es esencial para encontrar lo que funciona mejor para ti.

    <strong>8. Refuerza Positivamente tus Acciones:</strong>

    Cada vez que completes tu hábito, date una recompensa o reconocimiento. Esto puede ser algo tan simple como un momento de gratitud o un pequeño capricho.

    <strong>9. Evalúa tu Progreso Regularmente:</strong>

    Tómate un tiempo cada semana para reflexionar sobre cómo va tu rutina y si estás más cerca de convertir tu comportamiento en un hábito arraigado.
    Recuerda que la creación de una rutina efectiva es un proceso personal y único. Lo que funciona para una persona puede no funcionar para otra. Encuentra tu ritmo, sé paciente contigo mismo y celebra cada paso que das hacia tus objetivos.`,
    category_id: 'habitos'
  },
  {
    id: '14',
    title: 'Utiliza recordatorios',
    // description: 'Coloca recordatorios visuales o establece alarmas en tu teléfono para recordarte tus nuevos hábitos. Esto te ayudará a mantener el enfoque y la consistencia.',
    description: `Tu Aliado para la Consistencia

    Los recordatorios son una herramienta poderosa para mantener tus nuevos hábitos en primer plano de tu mente. Aquí te presentamos cómo puedes maximizar su efectividad:

    <strong>1. Elige Recordatorios Efectivos:</strong>

    No todos los recordatorios funcionan igual para todos. Algunas personas responden mejor a las notas adhesivas, mientras que otras prefieren las alarmas o notificaciones del teléfono.

    <strong>2. Ubicación Estratégica:</strong>

    Coloca recordatorios visuales en lugares donde seguramente los verás. Si tu hábito es beber más agua, pon una nota en la nevera o en tu escritorio.

    <strong>3. Tecnología a tu Favor:</strong>

    Utiliza la tecnología para establecer alarmas o notificaciones. Hay aplicaciones diseñadas para ayudarte a construir y mantener hábitos, aprovecha estas herramientas.

    <strong>4. Personaliza tus Recordatorios:</strong>

    Haz que tus recordatorios sean personales y significativos. Si estás intentando meditar más, tu recordatorio podría ser una cita inspiradora sobre la paz interior.

    <strong>5. Recordatorios Múltiples:</strong>

    No te limites a un solo recordatorio. Si estás tratando de hacer ejercicio regularmente, configura varios a lo largo del día para mantenerte motivado.

    <strong>6. Asociación con Rutinas Existentes:</strong>

    Vincula tus recordatorios con hábitos que ya tienes establecidos. Si siempre revisas el correo por la mañana, agrega el nuevo hábito justo después.

    <strong>7. Ajusta y Actualiza:</strong>

    A medida que tus hábitos evolucionan, tus recordatorios también deben hacerlo. Actualízalos para reflejar tu progreso y los nuevos desafíos que enfrentas.

    <strong>8. Mantén la Simplicidad:</strong>

    Los recordatorios deben ser simples y directos. Evita complicarlos con demasiados detalles que puedan resultar abrumadores.

    <strong>9. Evalúa su Efectividad:</strong>

    De vez en cuando, revisa si tus recordatorios siguen siendo efectivos. Si no es así, es hora de cambiarlos.

    <strong>10. Combinación de Métodos:</strong>

    Experimenta con una combinación de recordatorios visuales y auditivos para encontrar lo que mejor funciona para ti.
    Recuerda, el objetivo de los recordatorios es facilitar la incorporación de nuevos hábitos hasta que se conviertan en parte de tu comportamiento natural. Con el uso inteligente y estratégico de recordatorios, estarás en camino a lograr una mayor enfoque y consistencia en tus actividades diarias.`,
    category_id: 'habitos'
  },
  {
    id: '15',
    title: 'Identifica desencadenantes y recompensas',
    // description: 'Identifica qué desencadena tus hábitos actuales, ya sean buenos o malos, y busca formas de cambiar esos desencadenantes. Asimismo, identifica recompensas saludables que puedas asociar con tus nuevos hábitos para reforzar su adopción.',
    description: `La Fórmula para Transformar Hábitos

    Comprender los desencadenantes de tus hábitos y cómo las recompensas influyen en ellos es fundamental para realizar cambios duraderos. A continuación, te presento una guía detallada para ayudarte en este proceso:

    <strong>1. Reconoce tus Desencadenantes:</strong>

    Observa tus rutinas diarias y toma nota de los momentos que preceden a tus hábitos, tanto positivos como negativos. Esto puede incluir ciertos lugares, emociones, personas o actividades.

    <strong>2. Analiza los Patrones:</strong>

    Busca patrones en tus desencadenantes. ¿Hay ciertos factores que consistentemente resultan en comportamientos no deseados? ¿Qué tienen en común los desencadenantes de tus hábitos positivos?

    <strong>3. Crea Estrategias de Sustitución:</strong>

    Para los hábitos que deseas cambiar, identifica acciones alternativas que puedas realizar cuando te enfrentes a un desencadenante. Por ejemplo, si sueles comer dulces cuando estás estresado, prueba con una caminata rápida en su lugar.

    <strong>4. Diseña un Entorno Conducivo:</strong>

    Modifica tu entorno para reducir la presencia de desencadenantes negativos y aumentar los positivos. Si quieres reducir el tiempo frente a pantallas, guarda tus dispositivos electrónicos en un cajón después de cierta hora.

    <strong>5. Identifica Recompensas Saludables:</strong>

    Elige recompensas que sean beneficiosas y alineadas con tus objetivos a largo plazo. Si tu hábito deseado es hacer ejercicio, tu recompensa podría ser una sesión de relajación en un baño de espuma.

    <strong>6. Asocia Nuevos Hábitos con Recompensas:</strong>

    Vincula tus nuevos hábitos con recompensas inmediatas. Esto refuerza el comportamiento positivo y lo hace más atractivo para tu cerebro.

    <strong>7. Sé Consistente:</strong>

    Aplica tus desencadenantes y recompensas de manera consistente para fortalecer la asociación en tu mente. La repetición es clave para formar nuevos patrones de comportamiento.

    <strong>8. Reflexiona sobre tus Experiencias:</strong>

    Tómate un tiempo para reflexionar sobre cómo los desencadenantes y las recompensas afectan tus hábitos. ¿Te sientes más motivado? ¿Hay ajustes que podrías hacer para mejorar?

    <strong>9. Ajusta Según Sea Necesario:</strong>

    No todas las estrategias funcionarán perfectamente desde el principio. Esté abierto a experimentar y ajustar tus enfoques según sea necesario.

    <strong>10. Celebra tus Éxitos:</strong>

    Cada vez que resistas un desencadenante negativo o sigas un hábito positivo, celebra de alguna manera. Esto no solo es gratificante, sino que también solidifica el cambio en tu comportamiento.
    Al identificar y comprender tus desencadenantes y recompensas, puedes tomar el control de tus hábitos y dirigir tu comportamiento hacia tus metas y valores. Con paciencia y práctica, transformarás tus hábitos de manera efectiva y duradera.`,
    category_id: 'habitos'
  },
  // {
  //   id: '16',
  //   title: 'Visualiza el éxito',
  //   description: 'Visualízate a ti mismo teniendo éxito en la incorporación de tus nuevos hábitos. La visualización positiva puede ayudarte a mantenerte motivado y comprometido con tu objetivo',
  //   category_id: 'habitos'
  // },
  // {
  //   id: '17',
  //   title: 'Busca apoyo',
  //   description: 'Comparte tus objetivos con amigos, familiares o compañeros de trabajo que te puedan apoyar en tu viaje hacia el cambio de hábitos. La responsabilidad y el aliento pueden ser poderosos impulsores.',
  //   category_id: 'habitos'
  // },
  // {
  //   id: '18',
  //   title: 'Sé paciente y perdona tus errores',
  //   description: 'Cambiar hábitos lleva tiempo y es normal experimentar contratiempos en el camino. Sé compasivo contigo mismo y vuelve a enfocarte en tus objetivos si te desvías del camino.',
  //   category_id: 'habitos'
  // },
  // {
  //   id: '19',
  //   title: 'Evalúa y ajusta',
  //   description: 'Regularmente revisa tus hábitos y evalúa tu progreso. Si encuentras que algo no está funcionando, ajusta tu enfoque y prueba nuevas estrategias para lograr tus objetivos.',
  //   category_id: 'habitos'
  // },
  // {
  //   id: '20',
  //   title: 'Celebra tus éxitos',
  //   description: 'Reconoce y celebra tus logros, por pequeños que sean. Esto te ayudará a mantenerte motivado y a seguir adelante con tu viaje hacia una vida más saludable y productiva.',
  //   category_id: 'habitos'
  // },
  {
    id: '21',
    title: 'Prepárate adecuadamente',
    // description: 'Investiga el tema que vas a presentar y organiza tus ideas de manera clara y lógica. Cuanto mejor entiendas el tema, más confianza tendrás al hablar sobre él.',
    description: `El Secreto para una Presentación Exitosa

    La preparación es esencial cuando se trata de hablar en público. Aquí te presento una guía ampliada para asegurarte de que estás completamente preparado para tu próxima presentación:

    <strong>1. Investiga Profundamente:</strong>

    Dedica tiempo a investigar tu tema. No solo te quedes con la información superficial; busca estudios, estadísticas, y ejemplos que enriquezcan tu presentación.

    <strong>2. Conoce a tu Audiencia:</strong>

    Entiende quién estará escuchando. ¿Cuál es su nivel de conocimiento sobre el tema? ¿Qué esperan aprender o llevarse de tu presentación?

    <strong>3. Organiza tus Puntos Clave:</strong>

    Desarrolla una estructura clara para tu presentación. Comienza con una introducción que capte la atención, seguida de los puntos principales y una conclusión fuerte.

    <strong>4. Crea Materiales de Apoyo:</strong>

    Prepara diapositivas, folletos o cualquier otro material que pueda ayudar a transmitir tu mensaje de manera más efectiva.

    <strong>5. Practica tu Discurso:</strong>

    Ensayar es crucial. Practica tu presentación varias veces, preferiblemente frente a un espejo o un pequeño grupo de personas.

    <strong>6. Aprende de Memoria los Puntos Principales:</strong>

    Aunque no necesitas memorizar todo el discurso palabra por palabra, asegúrate de conocer los puntos principales para mantener el flujo de tu presentación.

    <strong>7. Controla el Tiempo:</strong>

    Asegúrate de que tu presentación se ajuste al tiempo asignado. Practica con un cronómetro para desarrollar un sentido del tiempo.

    <strong>8. Prepara Respuestas para Preguntas Potenciales:</strong>

    Piensa en preguntas que la audiencia podría hacer y prepara respuestas. Esto te ayudará a manejar la sesión de preguntas y respuestas con confianza.

    <strong>9. Trabaja en tu Lenguaje Corporal:</strong>

    Tu lenguaje corporal comunica tanto como tus palabras. Practica gestos y posturas que proyecten confianza y apertura.

    <strong>10. Realiza Simulacros:</strong>

    Si es posible, realiza un ensayo general en el lugar donde darás la presentación. Esto te ayudará a familiarizarte con el espacio y la acústica.

    <strong>11. Cuida tu Voz:</strong>

    Haz ejercicios de vocalización y asegúrate de descansar tu voz antes del gran día, especialmente si la presentación es larga.

    <strong>12. Mantén la Calma:</strong>

    Es normal sentir nervios. Aprende técnicas de relajación como la respiración profunda para mantener la calma antes y durante tu presentación.

    <strong>13. Acepta la Retroalimentación:</strong>

    Después de practicar, pide retroalimentación y úsala para mejorar tu presentación.

    <strong>14. Visualiza el Éxito:</strong>

    Imagina tu presentación yendo bien. La visualización positiva puede aumentar tu confianza y reducir la ansiedad.

    <strong>15. Prepárate para lo Inesperado:</strong>

    Ten planes de contingencia en caso de que algo no salga como esperabas, como problemas técnicos o interrupciones.
    Al seguir estos pasos, estarás bien preparado para hablar en público. Recuerda que la clave es la preparación y la práctica. Cuanto más te prepares, más confianza tendrás en tu capacidad para entregar una presentación impactante y memorable.`,
    category_id: 'hablar-en-pulico'
  },
  {
    id: '22',
    title: 'Conoce a tu audiencia',
    // description: 'Antes de tu presentación, averigua quiénes serán tus oyentes y adapta tu discurso a sus intereses y nivel de conocimiento sobre el tema.',
    description: `La Estrategia para una Presentación Persuasiva

    Entender a tu audiencia es crucial para el éxito de cualquier presentación. Aquí te presento una guía ampliada para ayudarte a conectar efectivamente con tus oyentes:

    <strong>1. Investiga a los Asistentes:</strong>

    Antes del evento, intenta obtener una lista de los asistentes y su trasfondo. ¿Son expertos en el tema, o es nuevo para ellos? ¿Qué profesiones tienen? Esta información te ayudará a ajustar tu lenguaje y ejemplos.

    <strong>2. Comprende sus Intereses:</strong>

    Averigua qué aspectos del tema podrían ser más relevantes o interesantes para tu audiencia. Esto te permitirá enfocar tu presentación en las áreas que captarán su atención.

    <strong>3. Adapta tu Contenido:</strong>

    Basándote en tu investigación, adapta tu presentación para que resuene con tus oyentes. Usa ejemplos y casos de estudio que sean pertinentes para ellos.

    <strong>4. Prepara Preguntas Interactivas:</strong>

    Planifica preguntas que puedas hacer durante la presentación para involucrar a tu audiencia y hacerla participativa.

    <strong>5. Considera las Diferencias Culturales:</strong>

    Si tu audiencia es internacional, ten en cuenta las diferencias culturales que pueden influir en cómo se recibe tu mensaje.

    <strong>6. Ajusta tu Estilo de Presentación:</strong>

    Algunas audiencias prefieren una presentación formal, mientras que otras responden mejor a un enfoque más casual y conversacional. Elige el estilo que mejor se adapte a tus oyentes.

    <strong>7. Utiliza el Lenguaje Apropiado:</strong>

    Evita jergas y términos técnicos si tu audiencia no está familiarizada con ellos. Si es necesario usarlos, asegúrate de explicarlos claramente.

    <strong>8. Diseña una Presentación Visual Atractiva:</strong>

    Usa diapositivas y otros materiales visuales que sean atractivos y fáciles de entender para todos los niveles de conocimiento.

    <strong>9. Practica la Empatía:</strong>

    Ponerte en el lugar de tu audiencia te ayudará a crear una conexión más fuerte y a entregar un mensaje más impactante.

    <strong>10. Solicita Retroalimentación:</strong>

    Si es posible, pide comentarios sobre tu presentación a un pequeño grupo representativo de tu audiencia antes del evento principal.

    <strong>11. Sé Flexible Durante la Presentación:</strong>

    Esté preparado para adaptar tu discurso sobre la marcha si notas que ciertos puntos no están resonando como esperabas.

    <strong>12. Ofrece Materiales Adicionales:</strong>

    Proporciona recursos adicionales, como artículos o enlaces a sitios web, para aquellos que deseen profundizar más en el tema.

    <strong>13. Evalúa y Aprende:</strong>

    Después de tu presentación, reflexiona sobre lo que funcionó y lo que no. Usa esta información para mejorar tus futuras presentaciones.
    Al conocer a tu audiencia y adaptar tu presentación a sus necesidades e intereses, no solo mejorarás la efectividad de tu discurso, sino que también construirás una relación más sólida con tus oyentes. Esto es esencial para comunicar tu mensaje de manera efectiva y dejar una impresión duradera.`,
    category_id: 'hablar-en-pulico'
  },
  {
    id: '23',
    title: 'Practica tu discurso',
    description: `<strong>La Piedra Angular de una Presentación Efectiva</strong>

    La práctica es un componente esencial en el arte de hablar en público. No solo se trata de repasar las palabras que dirás, sino de construir la confianza y la fluidez necesarias para comunicar tu mensaje con claridad y convicción. Al practicar, no solo memorizas el contenido, sino que también afinas el tono, el ritmo y las pausas, elementos cruciales para mantener la atención de la audiencia y transmitir emociones. Además, la práctica te permite identificar y corregir cualquier dificultad lingüística o complejidad en la presentación, asegurando que cada palabra tenga el impacto deseado. Al ensayar tu discurso, también puedes experimentar con diferentes formas de expresión, descubriendo qué gestos y movimientos complementan tus palabras y te ayudan a establecer una conexión más profunda con tu audiencia. Recuerda que una presentación exitosa es más que solo hablar; es una performance que requiere preparación y ensayo, similar a un músico que se prepara para un concierto o un actor antes de subir al escenario. Por lo tanto, dedica tiempo a practicar tu presentación en diferentes contextos y ante diversas audiencias, si es posible. Esto te brindará la oportunidad de recibir retroalimentación constructiva y ajustar tu discurso para lograr la máxima efectividad. Al final, la práctica te equipará con la habilidad para manejar imprevistos y preguntas inesperadas, permitiéndote entregar una presentación que no solo informe, sino que también inspire y motive a tus oyentes.`,
    category_id: 'hablar-en-pulico'
  },
  {
    id: '24',
    title: 'Controla tu lenguaje corporal',
    // description: 'Mantén una postura erguida y gestos naturales. El lenguaje corporal puede transmitir confianza y credibilidad, así que asegúrate de mantener contacto visual con tu audiencia y evitar gestos nerviosos como tamborilear con los dedos o cruzar los brazos.',
    description: `El lenguaje corporal es un aspecto fundamental de la comunicación, especialmente cuando se trata de hablar en público. Un buen control del lenguaje corporal no solo mejora la percepción que los demás tienen de ti, sino que también puede influir en tu propia psicología, aumentando tu confianza y capacidad para transmitir tu mensaje de manera efectiva. Mantener una postura erguida y abierta no solo te hace parecer más seguro, sino que también te permite respirar mejor, lo que es crucial para mantener la calma y la claridad al hablar. Los gestos naturales y expresivos pueden ayudar a enfatizar tus puntos y hacer que tu discurso sea más dinámico y atractivo. Sin embargo, es importante que estos gestos sean congruentes con lo que estás diciendo; de lo contrario, podrían distraer o confundir a tu audiencia.

    El contacto visual es otro componente clave del lenguaje corporal efectivo. Al mirar a los miembros de tu audiencia en los ojos, no solo estableces una conexión más personal, sino que también demuestras apertura y sinceridad. Por supuesto, esto debe hacerse de manera equilibrada, distribuyendo tu atención por toda la sala para incluir a todos los presentes. Evita mirar a una sola persona o un punto fijo por demasiado tiempo, ya que esto puede ser incómodo o dar la impresión de que estás leyendo un guion.

    Es igualmente importante ser consciente de los gestos nerviosos o las posturas cerradas, como cruzar los brazos o jugar con un bolígrafo. Estos comportamientos pueden transmitir ansiedad o defensividad, lo que puede socavar tu autoridad o la fuerza de tu mensaje. Si descubres que tienes tendencia a estos hábitos, trabaja conscientemente para minimizarlos, tal vez reemplazándolos con movimientos más positivos y controlados.

    En resumen, el lenguaje corporal es una herramienta poderosa que, cuando se utiliza correctamente, puede reforzar tu discurso y ayudarte a comunicarte de manera más efectiva. Con práctica y autoconciencia, puedes aprender a controlar tu lenguaje corporal para que trabaje a tu favor, permitiéndote entregar presentaciones convincentes y memorables.`,
    category_id: 'hablar-en-pulico'
  },
  {
    id: '25',
    title: 'Controla tu respiración',
    // description: 'Respira profundamente y de manera regular para ayudar a calmar los nervios y controlar tu ritmo al hablar. Practica técnicas de respiración antes de tu presentación para sentirte más relajado.',
    description: `El control de la respiración es una técnica poderosa que puede tener un impacto significativo en tu capacidad para hablar en público con calma y claridad. Respirar profundamente no solo oxigena tu cerebro, mejorando tu enfoque y claridad de pensamiento, sino que también puede tener un efecto calmante en tu sistema nervioso, ayudándote a reducir la ansiedad y el estrés. Antes de tu presentación, dedica unos momentos para practicar la respiración diafragmática, una técnica que implica respirar profundamente desde el vientre en lugar de superficialmente desde el pecho. Esto te ayudará a mantener un ritmo de habla constante y evitará que tu voz se quiebre bajo la presión.

    Además, la respiración controlada puede ser una herramienta útil durante tu presentación para recuperar tu compostura si te sientes abrumado o pierdes el hilo de tus pensamientos. Al tomar una pausa consciente y respirar profundamente, puedes reconectar con tu discurso y continuar con mayor confianza. Practica estas técnicas regularmente, no solo en preparación para hablar en público, sino también como parte de tu rutina diaria. Con el tiempo, descubrirás que la capacidad de controlar tu respiración se convierte en una segunda naturaleza, proporcionándote una base sólida para enfrentar cualquier desafío de comunicación con serenidad y presencia.`,
    category_id: 'hablar-en-pulico'
  },
  // {
  //   id: '26',
  //   title: 'Habla con claridad y en un volumen adecuado',
  //   description: 'Asegúrate de pronunciar claramente y hablar a un volumen que todos puedan escuchar. Evita hablar demasiado rápido y utiliza pausas estratégicas para enfatizar puntos importantes.',
  //   category_id: 'hablar-en-pulico'
  // },
  // {
  //   id: '27',
  //   title: 'Utiliza recursos visuales de manera efectiva',
  //   description: 'Si utilizas diapositivas u otros recursos visuales, asegúrate de que sean claros y concisos. No sobrecargues tus diapositivas con texto y utiliza imágenes o gráficos para ilustrar tus puntos.',
  //   category_id: 'hablar-en-pulico'
  // },
  // {
  //   id: '28',
  //   title: 'Mantén el contacto con tu audiencia',
  //   description: 'Interactúa con tu audiencia haciendo preguntas, contando anécdotas o invitando a comentarios. Esto ayuda a mantener su atención y crear una conexión más personal.',
  //   category_id: 'hablar-en-pulico'
  // },
  // {
  //   id: '29',
  //   title: 'Maneja las preguntas con calma',
  //   description: 'Prepárate para responder preguntas de la audiencia de manera clara y concisa. Si no tienes la respuesta, no tengas miedo de admitirlo y ofrecer buscar más información después.',
  //   category_id: 'hablar-en-pulico'
  // },
  // {
  //   id: '30',
  //   title: 'Evalúa y mejora',
  //   description: 'Después de tu presentación, reflexiona sobre lo que funcionó bien y en qué áreas puedes mejorar. Utiliza esta retroalimentación para perfeccionar tus habilidades de hablar en público en futuras ocasiones.',
  //   category_id: 'hablar-en-pulico'
  // },
  {
    id: '31',
    title: 'Pasión y compromiso',
    // description: 'Ser padre requiere pasión y compromiso. Debes estar dispuesto a dedicar tiempo y esfuerzo a tus hijos, y a comprometerte con su bienestar físico, emocional y mental.',
    description: `La paternidad es una de las experiencias más enriquecedoras y desafiantes que una persona puede tener. La pasión y el compromiso son fundamentales para guiar y apoyar el desarrollo de tus hijos. Como padre, tu rol va más allá de proveer necesidades básicas; implica estar emocionalmente disponible, ser un modelo a seguir y fomentar un ambiente donde tus hijos puedan crecer y prosperar. La pasión se refleja en la alegría y el entusiasmo con el que participas en sus vidas, celebrando sus logros y apoyándolos en sus desafíos. El compromiso se muestra en la constancia y la dedicación con la que te involucras en su educación, sus actividades y su bienestar general.

    Ser padre significa también ser paciente y comprensivo, sabiendo que cada niño es único y que el camino hacia la madurez está lleno de aprendizajes tanto para ellos como para ti. Requiere una inversión de tiempo no solo en la cantidad sino en la calidad, asegurándote de que cada momento que pasas con tus hijos sea significativo y constructivo. Es comprometerte a enseñarles valores, habilidades y actitudes que les servirán para toda la vida. Y aunque la paternidad viene con sus desafíos, la pasión por ver a tus hijos convertirse en individuos felices, saludables y realizados hace que cada esfuerzo valga la pena. En última instancia, ser padre es un viaje continuo de amor, aprendizaje y crecimiento mutuo.`,
    category_id: 'para-padres'
  },
  {
    id: '32',
    title: 'Comunicación abierta',
    // description: 'Fomenta una comunicación abierta y honesta con tus hijos. Escúchalos activamente, sé comprensivo y muestra interés genuino en lo que tienen que decir.',
    description: `La comunicación abierta es el pilar de una relación saludable y sólida entre padres e hijos. No se trata solo de hablar y escuchar, sino de crear un espacio seguro donde los niños se sientan valorados y entendidos. Una comunicación efectiva implica escuchar no solo con los oídos, sino con el corazón, mostrando empatía y comprensión hacia sus sentimientos y perspectivas. Al fomentar un diálogo abierto, estás enseñando a tus hijos habilidades vitales de comunicación que les servirán en todas las áreas de la vida. Además, al estar disponible y ser receptivo a sus pensamientos y preocupaciones, refuerzas su autoestima y fomentas su confianza para expresarse. Este tipo de interacción construye una base de confianza mutua y respeto, elementos esenciales para una relación duradera y amorosa. Recuerda que la comunicación es un proceso bidireccional; al mismo tiempo que animas a tus hijos a compartir, también es importante compartir tus propios pensamientos y sentimientos. Esto les muestra que la comunicación es una calle de doble sentido y que sus opiniones son tan valiosas como las tuyas. Con una comunicación abierta y honesta, puedes guiar a tus hijos a través de los desafíos de la vida, proporcionándoles el apoyo y la orientación que necesitan para prosperar.`,
    category_id: 'para-padres'
  },
  {
    id: '33',
    title: 'Establece límites y expectativas claras',
    // description: 'Es importante establecer límites y expectativas claras para tus hijos desde una edad temprana. Esto les ayuda a sentirse seguros y a desarrollar habilidades de autorregulación.',
    description: `Establecer límites y expectativas claras es una parte crucial de la educación que proporciona a los niños un sentido de estructura y seguridad. Los límites actúan como guías que ayudan a los niños a comprender lo que se espera de ellos, lo que está permitido y lo que no. Al tener reglas consistentes, los niños aprenden a navegar dentro de parámetros seguros y a tomar decisiones responsables. Las expectativas claras, por otro lado, les dan a los niños metas alcanzables y un entendimiento de lo que pueden lograr. Esto no solo fomenta su desarrollo de habilidades de autorregulación, sino que también promueve la autoestima al alcanzar las metas establecidas.

    Además, cuando los padres comunican y refuerzan estos límites y expectativas de manera positiva y firme, los niños aprenden a confiar y a respetar la autoridad. También aprenden a manejar la frustración y a lidiar con las consecuencias de sus acciones, lo cual es esencial para su desarrollo emocional y social. Es importante que los padres sean modelos a seguir en la consistencia y el respeto de los límites que establecen, ya que los niños a menudo imitan el comportamiento de los adultos en sus vidas. Al final, los límites y expectativas no solo preparan a los niños para la vida cotidiana, sino que también les enseñan valores fundamentales como la disciplina, el respeto y la responsabilidad personal.`,
    category_id: 'para-padres'
  },
  {
    id: '34',
    title: 'Modela comportamientos positivos',
    description: `Ser un modelo a seguir para tus hijos es una responsabilidad que trasciende las palabras; se trata de acciones y actitudes que se viven y se demuestran en la vida cotidiana. Los niños son observadores atentos y aprenden más de lo que ven que de lo que se les dice. Al modelar comportamientos positivos, estás sentando las bases para que ellos desarrollen habilidades sociales y emocionales saludables. El respeto se muestra no solo en cómo tratas a los demás, sino también en cómo te respetas a ti mismo y mantienes tus límites. La empatía se enseña al ponerse en el lugar del otro y responder con comprensión y cuidado. La responsabilidad se demuestra cumpliendo con tus compromisos y admitiendo errores cuando ocurren. La resiliencia se refleja en cómo enfrentas los desafíos y te recuperas de los contratiempos.

    Al vivir estos valores, estás proporcionando a tus hijos un ejemplo tangible de cómo navegar por el mundo de manera efectiva y compasiva. Esto incluye también la honestidad, la integridad y la generosidad, cualidades que se convierten en parte del carácter de tus hijos a medida que crecen y aprenden de ti. Recuerda que cada acción y cada elección es una oportunidad para enseñar a tus hijos sobre la vida y sobre cómo ser la mejor versión de sí mismos. Al final, los comportamientos que modelas formarán el núcleo de su desarrollo moral y ético, y serán una influencia duradera en la persona que se convertirán.`,
    category_id: 'para-padres'
  },
  {
    id: '35',
    title: 'Tiempo de calidad',
    // description: 'Dedica tiempo de calidad a tus hijos. Participa en actividades que disfruten juntos, como jugar, leer, hacer manualidades o simplemente hablar y compartir historias.',
    description: `El tiempo de calidad con tus hijos es un regalo invaluable que tiene un impacto profundo y duradero en su bienestar emocional y desarrollo cognitivo. No se trata solo de la cantidad de tiempo que pasas con ellos, sino de la calidad de esos momentos compartidos. Participar activamente en actividades que ambos disfruten, como juegos, lectura o manualidades, crea recuerdos preciosos y fortalece el vínculo entre padres e hijos. Además, estas interacciones son oportunidades para enseñar habilidades vitales, como la resolución de problemas, la creatividad y la comunicación efectiva. Al estar presente y comprometido, demuestras a tus hijos que son una prioridad en tu vida, lo que les brinda seguridad y confianza.

    Más allá de las actividades lúdicas, el tiempo de calidad también incluye conversaciones significativas donde los niños pueden expresar sus pensamientos y sentimientos. Escuchar atentamente y compartir tus propias experiencias fomenta un ambiente de confianza y apertura. Este tipo de intercambio ayuda a los niños a desarrollar empatía y comprensión, y les enseña el valor de la comunicación en las relaciones. Al final, el tiempo de calidad no se mide por minutos u horas, sino por el impacto positivo que tiene en la vida de tus hijos, ayudándoles a crecer como individuos seguros, felices y bien adaptados.`,
    category_id: 'para-padres'
  },
  // {
  //   id: '36',
  //   title: 'Apoya sus intereses y pasiones',
  //   description: 'Fomenta los intereses y pasiones de tus hijos y bríndales apoyo en sus actividades extracurriculares y hobbies. Esto les ayuda a desarrollar confianza en sí mismos y a descubrir sus fortalezas.',
  //   category_id: 'para-padres'
  // },
  // {
  //   id: '37',
  //   title: 'Sé consistente y predecible',
  //   description: 'Mantén la consistencia y la previsibilidad en tus acciones y decisiones como padre. Los niños se sienten seguros cuando saben qué esperar de sus padres.',
  //   category_id: 'para-padres'
  // },
  // {
  //   id: '38',
  //   title: 'Fomenta la independencia',
  //   description: 'Permíteles a tus hijos explorar y aprender por sí mismos. Fomenta la independencia gradualmente a medida que crecen, dándoles responsabilidades y oportunidades para tomar decisiones.',
  //   category_id: 'para-padres'
  // },
  // {
  //   id: '39',
  //   title: 'Sé un apoyo emocional',
  //   description: 'Está presente para tus hijos en los buenos y malos momentos. Ofrece consuelo, aliento y apoyo emocional cuando lo necesiten, y valida sus sentimientos.',
  //   category_id: 'para-padres'
  // },
  // {
  //   id: '40',
  //   title: 'Cuida de ti mismo',
  //   description: 'No te olvides de cuidar tu propio bienestar físico, emocional y mental. Un padre feliz y saludable está mejor equipado para criar a sus hijos de manera efectiva.',
  //   category_id: 'para-padres'
  // },
  {
    id: '41',
    title: 'Establece metas claras y alcanzables',
    description: `Establecer metas claras y alcanzables es un componente esencial para mantener la motivación y lograr el éxito. Al definir objetivos específicos, no solo proporcionas una dirección clara para tus esfuerzos, sino que también creas un estándar por el cual puedes medir tu progreso. Estas metas deben ser lo suficientemente desafiantes para impulsarte a avanzar, pero también lo suficientemente realistas para que puedas alcanzarlas sin sentirte abrumado. Al hacer tus objetivos medibles, puedes celebrar cada pequeño éxito en el camino, lo que a su vez alimenta tu impulso para continuar.

    Además, es importante que tus metas estén alineadas con tus valores y pasiones, ya que esto te proporcionará una fuente de motivación intrínseca. Cuando trabajas hacia algo que tiene un significado personal profundo, es más probable que mantengas el compromiso incluso cuando enfrentes obstáculos. También es útil desglosar tus metas más grandes en submetas más pequeñas y manejables. Esto te permite concentrarte en el siguiente paso inmediato en lugar de sentirte intimidado por la magnitud del objetivo final.

    Recuerda que el camino hacia tus metas es tan importante como el resultado. Aprende de cada experiencia, ajusta tus planes según sea necesario y no tengas miedo de establecer nuevas metas a medida que creces y cambian tus circunstancias. Con metas claras y alcanzables, tienes el poder de dar forma a tu futuro y alcanzar tu máximo potencial.`,
    category_id: 'motivacion'
  },
  {
    id: '42',
    title: 'Encuentra tu "por qué"',
    description: `Encontrar tu “por qué” es sumergirse en las profundidades de tus motivaciones personales y descubrir la esencia de tus aspiraciones. Es un viaje introspectivo que va más allá de la superficie de cualquier objetivo superficial, tocando el núcleo de tus valores y creencias más arraigados. Este “por qué” es tu brújula interna, que te guía a través de las tormentas de la duda y la indecisión, y te proporciona la fuerza para superar los obstáculos. No es solo una razón; es la pasión que enciende tu espíritu y da vida a tus sueños.

    Cuando conectas emocionalmente con tus metas, transformas la lucha en un acto de amor propio y dedicación. Cada paso hacia tu meta se convierte en una afirmación de tu identidad y un compromiso con la vida que deseas crear. Ya sea que busques el éxito en tu carrera, la armonía en tus relaciones o la salud en tu vida diaria, tu “por qué” es el poderoso motor que impulsa cada acción. Es la voz que susurra “puedes hacerlo” cuando el camino se vuelve difícil, y la fuerza que te levanta cuando caes.

    Por lo tanto, tómate el tiempo para reflexionar y articular claramente este “por qué”. Escríbelo, háblalo, vívelo. Que sea el fundamento de tus planes y la inspiración para tus acciones. Y recuerda, cuando tu “por qué” es claro, tu “cómo” se vuelve mucho más fácil. Con una razón convincente detrás de tus metas, no hay límite para lo que puedes lograr. Tu “por qué” no solo te motiva, sino que también atrae a otros a tu causa, creando un círculo virtuoso de inspiración y logros compartidos. En última instancia, encontrar tu “por qué” es descubrir el regalo de una vida con propósito.`,
    category_id: 'motivacion'
  },
  {
    id: '43',
    title: 'Crea un plan de acción',
    description: `Crear un plan de acción es como trazar un mapa para un viaje; te proporciona una ruta clara hacia tu destino deseado. Un plan bien estructurado descompone tus metas en tareas manejables, asignando plazos y recursos específicos para cada una. Al hacerlo, transformas tus visiones en una serie de pasos prácticos y alcanzables, lo que facilita el seguimiento de tu progreso y te mantiene motivado a medida que completas cada etapa.

    Comienza por definir tu objetivo final y luego trabaja hacia atrás, estableciendo hitos intermedios que actúen como marcadores de progreso. Para cada hito, identifica las acciones específicas necesarias, quién será responsable de cada una, y cuándo deben completarse. Considera los posibles obstáculos que podrías encontrar y desarrolla estrategias para superarlos. Además, asegúrate de incluir momentos de reflexión para evaluar cómo va el proceso y hacer ajustes según sea necesario.

    Un plan de acción también debe ser flexible; la vida es impredecible y los planes pueden necesitar cambiar. Mantén una mentalidad abierta y adaptable, permitiéndote modificar tu plan sin perder de vista tus metas generales. Al final, un plan de acción sólido es tu guía en el camino hacia el éxito, proporcionándote la estructura y el enfoque necesarios para convertir tus sueños en realidad. Con cada paso que das siguiendo tu plan, te acercas más a la realización de tus ambiciones y al cumplimiento de tu potencial.`,
    category_id: 'motivacion'
  },
  {
    id: '44',
    title: 'Celebra tus logros',
    description: `Celebrar tus logros es un aspecto vital en el camino hacia tus metas, ya que actúa como un refuerzo positivo y aumenta tu motivación intrínseca. Cada éxito, no importa cuán pequeño, es un paso adelante en tu viaje y merece ser reconocido. Estos momentos de celebración son oportunidades para reflexionar sobre el trabajo duro que has invertido y para apreciar el progreso que has hecho. Además, al reconocer tus logros, estás construyendo un registro de tus éxitos que puede servir como un recordatorio de tu capacidad para superar desafíos y alcanzar objetivos.

    La celebración de tus éxitos también puede ser compartida con amigos, familiares o colegas, lo que no solo te proporciona un sentido de comunidad y apoyo, sino que también te permite inspirar y motivar a otros. Ya sea que te des un pequeño capricho, compartas tu logro en las redes sociales o simplemente te tomes un momento para disfrutar internamente de tu éxito, cada celebración es un acto de autoafirmación. Además, estas celebraciones pueden convertirse en rituales que marcan el final de una etapa y el comienzo de la siguiente, dándote un impulso renovado para seguir adelante.

    Recuerda que el camino hacia el logro de metas a menudo está lleno de altibajos, y es fácil desanimarse con los contratiempos. Sin embargo, al hacer una pausa para celebrar cada logro, te recuerdas a ti mismo que cada esfuerzo cuenta y que estás avanzando, paso a paso, hacia tus sueños más grandes. Así que, no subestimes el poder de una celebración, por más modesta que sea, porque es el combustible que mantiene encendida la llama de tu determinación y pasión.`,
    category_id: 'motivacion'
  },
  {
    id: '45',
    title: 'Busca inspiración',
    description: `Buscar inspiración es vital para mantener la llama de la motivación encendida, especialmente en momentos de desánimo o duda. Los modelos a seguir y las historias inspiradoras actúan como faros de luz en nuestro camino, recordándonos que los obstáculos pueden ser superados y que nuestros sueños son alcanzables. Estas figuras ejemplares, ya sean personajes históricos, líderes contemporáneos, o incluso héroes de ficción, encarnan las cualidades y los logros a los que aspiramos. Sus vidas y narrativas nos ofrecen valiosas lecciones sobre perseverancia, coraje y la importancia de mantenerse fieles a uno mismo.

    Además, la inspiración puede venir de diversas fuentes: un libro que te conmueve, una película que te desafía, una obra de arte que te cautiva o una simple conversación que te abre los ojos a nuevas posibilidades. Mantén tu mente y tu corazón abiertos a estas experiencias, y permíteles infundir en ti un renovado sentido de propósito y pasión. Cuando te sientas desanimado, recuerda estas historias y utiliza su energía para reavivar tu determinación. La inspiración es un recurso inagotable; solo necesitas buscarla, y encontrarás innumerables ejemplos de tenacidad y resiliencia que te impulsarán a seguir adelante en tu propio viaje.`,
    category_id: 'motivacion'
  },
  // {
  //   id: '46',
  //   title: 'Mantén una mentalidad positiva',
  //   description: 'Cultiva pensamientos positivos y practica el autocuidado. La manera en que te hablas a ti mismo puede tener un gran impacto en tu motivación y autoestima.',
  //   category_id: 'motivacion'
  // },
  // {
  //   id: '47',
  //   title: 'Persiste ante los obstáculos',
  //   description: 'Espera desafíos en el camino hacia tus metas y no te desanimes cuando surjan. En lugar de rendirte, busca soluciones creativas y mantén tu determinación.',
  //   category_id: 'motivacion'
  // },
  // {
  //   id: '48',
  //   title: 'Rodéate de personas positivas',
  //   description: 'El apoyo de amigos, familiares o colegas puede ser invaluable para mantenerte motivado. Busca personas que te animen y te apoyen en tus esfuerzos.',
  //   category_id: 'motivacion'
  // },
  // {
  //   id: '49',
  //   title: 'Visualiza el éxito',
  //   description: 'Imagina cómo se sentirá lograr tus metas. Visualizarte alcanzando el éxito puede aumentar tu motivación y ayudarte a superar los momentos difíciles.',
  //   category_id: 'motivacion'
  // },
  // {
  //   id: '50',
  //   title: 'Aprende de los fracasos',
  //   description: 'En lugar de ver los contratiempos como derrotas, considera cada uno como una oportunidad de aprendizaje. Analiza lo que salió mal y utiliza esa información para ajustar tu enfoque y seguir adelante con más determinación.',
  //   category_id: 'motivacion'
  // },
];
