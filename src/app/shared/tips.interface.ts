export interface Category {
  id: string;
  name: string;
  subtitle?: string;
  color?: string;
  img?: string;
}

export interface Tip {
  id: string;
  title: string;
  description: string;
  category_id: string;
}
